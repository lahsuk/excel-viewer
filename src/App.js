import logo from './logo.svg';
import './App.css';
import React from 'react';

import { UploadViewer } from './Excel.js';

function App() {
  return (
    <>
      <UploadViewer />
    </>
  )
}

export default App;
