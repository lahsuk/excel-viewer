import React from 'react';

import "./Excel.css";

const BASE_API = "http://localhost:8000";


const excelReducer = (state, action) => {
  switch (action.type) {
    case "UPLOAD_INIT":
      return {
        ...state,
        isLoading: true,
        data: [],
      }
    case "UPLOAD_SUCCESS":
      return {
        ...state,
        isLoading: false,
        isError: false,
        filename: action.filename,
      }
    case "UPLOAD_FAILURE":
      return {
        ...state,
        isLoading: false,
        isError: true,
        errorMsg: action.errorMsg,
      }
    case "INIT_LOAD_EXCEL_DATA":
      return {
        ...state,
        isLoading: true,
      }
    case "LOAD_EXCEL_DATA":
      return {
        ...state,
        isError: false,
        isLoading: false,
        data: action.data,
      }
    case "LOAD_EXCEL_DATA_FAILED":
      return {
        ...state,
        isError: true,
        isLoading: false,
        errorMsg: action.errorMsg,
      }
    default:
      throw new Error();
  }
}

function UploadViewer() {
  const [excelData, dispatchExcelData] = React.useReducer(
    excelReducer,
    { "data": [], "filename": "", "isLoading": false, "isError": false, "errorMsg": "" }
  )

  function setFormName(filename) {
    dispatchExcelData({ "type": "UPLOAD_SUCCESS", "filename": filename });
  }

  React.useEffect(() => {
    if (!excelData.filename) return;

    dispatchExcelData({ "type": "INIT_LOAD_EXCEL_DATA" });

    fetch(`${BASE_API}/xcel/${excelData.filename}`)
      .then(response =>
        response.json()
      )
      .then(data => {
        dispatchExcelData(
          { "type": "LOAD_EXCEL_DATA", "data": data.data }
        )
      })
      .catch(error => {
        return dispatchExcelData({ "type": "LOAD_EXCEL_DATA_FAILED", "errorMsg": "Error reading file." })
      })
  }, [excelData.filename])

  return (
    <>
      <UploadForm setFormName={setFormName} dispatchExcelData={dispatchExcelData} />
      <ExcelViewer excelData={excelData} />
      {excelData.isError && <p>{excelData.errorMsg}</p>}
    </>
  )
}

function UploadForm({ setFormName, dispatchExcelData }) {
  const submitForm = (event) => {
    const formData = new FormData();
    const fileField = document.querySelector('input[type="file"]');

    formData.append('file', fileField.files[0]);
    dispatchExcelData({ "type": "UPLOAD_INIT" });

    event.preventDefault();
    fetch(`${BASE_API}/upload`, { method: "POST", body: formData })
      .then(response => response.json())
      .then(data => {
        if (data.error) {
          return dispatchExcelData({ "type": "UPLOAD_FAILURE", "errorMsg": data.error })
        } else {
          return setFormName(data.name);
        }
      })
      .catch(error => console.debug(error));
  }

  const updateSelectedFile = (event) => {
    const fileName = document.getElementById("userfile").files[0].name;
    var nextSibling = event.target.nextElementSibling
    nextSibling.innerText = fileName
  }

  return (
    <div class="section">
      <form class="uploadform" onSubmit={submitForm}>
        <div class="custom-file">
          <input type="file" class="custom-file-input" onChange={updateSelectedFile} id="userfile" />
          <label class="custom-file-label" htmlFor="userfile" >Choose file</label>
        </div>
        <input type="submit" value="Upload" class="btn btn-primary" style={{ "marginLeft": "10px" }} />
      </form>
    </div>
  )
}

function ExcelViewer({ excelData }) {
  return (
    <>
      {
        excelData.isLoading ?
          (<div class="d-flex justify-content-center" style={{ "padding": "20px" }}><div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
          </div></div>)
          :
          (<table class="table table-bordered">
            <tbody>
              {
                excelData.data.map(row => {
                  return (
                    <tr>
                      {row.map(value => {
                        return <td {...value.table} style={value.style}>{value.value}</td>
                      })}
                    </tr>
                  )
                })
              }
            </tbody>
          </table>)
      }
    </>
  )
}

export { UploadViewer };